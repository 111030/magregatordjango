document.getElementById("sign_user").onclick = function() {
   //alert("Im here");
   var sign_form = document.getElementById("form_login");
   var form_elems = sign_form.elements;
   var username = form_elems.namedItem("username");
   var password = form_elems.namedItem("password");
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         var response_json = JSON.parse(xhttp.responseText);
	 var response_value = response_json.username_error;
	 if (response_value) {
            $("#username_error").text(response_value);
	 }
	 else {
            response_value = response_json.password_error;
	    if (response_value) {
	       $("#password_error").text(response_value);
	    }
	    else {
	       response_value = response_json.username;
	       $("#auth_out").toggle();
	       $("#auth_in").toggle();
	       $("#user_message").text("Здраво "+response_value);
	       $("#sign_in_modal").modal("toggle");

	    }
	 }

      }
   };
   xhttp.open('POST', 'login/', true);
   var csrftoken = getCookie('csrftoken');
   xhttp.setRequestHeader("X-CSRFToken", csrftoken);
   xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   var uname_part = "username="+username.value;
   var pass_part = "password="+password.value;
   xhttp.send(uname_part+"&"+pass_part);


};

document.getElementById("register_user").onclick = function() {
   var reg_form = document.getElementById("form_register");
   //alert("The form is submited")
   var form_elems = reg_form.elements;
   var username = form_elems.namedItem("r_username");
   var password = form_elems.namedItem("r_password");
   var email = form_elems.namedItem("email");
   var name = form_elems.namedItem("name");
   var surname = form_elems.namedItem("surname");
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         var response_json = JSON.parse(xhttp.responseText);
	 var response_value = response_json.error_message;
	 if (response_value == "Success") {
            alert("Success");
	    $("#register_modal").modal("toggle");
	 }
	 else {
            $("#error_message").text(response_value);
	 }
      }
   };
   xhttp.open("POST", "register/", true);
   var csrftoken = getCookie('csrftoken');
   xhttp.setRequestHeader("X-CSRFToken", csrftoken);
   xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   var uname_part = "r_username="+username.value;
   var pass_part = "r_password="+password.value;
   var email_part = "email="+email.value;
   var name_part = "name="+name.value;
   var sur_part = "surname="+surname.value;
   xhttp.send(uname_part+"&"+pass_part+"&"+email_part+"&"+name_part+"&"+sur_part);
	
};

document.getElementById("btn_logout").onclick = function() {
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         //
	 $("#auth_in").toggle();
	 $("#auth_out").toggle();
      }
   };
   xhttp.open("POST", "logout/", true);
   var csrftoken = getCookie('csrftoken');
   xhttp.setRequestHeader("X-CSRFToken", csrftoken);
   xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   xhttp.send();
};

function getCookie(name) {
   var cookieValue = null;
   if (document.cookie && document.cookie != '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
         var cookie = jQuery.trim(cookies[i]);
	 if (cookie.substring(0, name.length + 1) == (name + '=')) {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	    break;
	 }
      }
   }
   return cookieValue;
}
