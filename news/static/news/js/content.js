document.getElementById("post_comment").onclick = function() {
   var xhttp = new XMLHttpRequest();
   var username= $("#comment_form_user").text();
   var comment = $("#comment").val();
   //alert(username);
   //alert(comment);
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         var response_html = xhttp.responseText;
	 $("#comments").prepend(response_html);
	 //alert(response_value);
      }
   };
   xhttp.open("POST", "postComment/", true);
   var csrftoken = getCookie('csrftoken');
   xhttp.setRequestHeader("X-CSRFToken", csrftoken);
   xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   var user_part="username="+username;
   var comment_part="comment="+comment;
   xhttp.send(user_part+"&"+comment_part);
};

function getCookie(name) {
   var cookieValue = null;
   if (document.cookie && document.cookie != '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
         var cookie = jQuery.trim(cookies[i]);
	 if (cookie.substring(0, name.length + 1) == (name + '=')) { 
	    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	    break;
	 }
      }
   }
   return cookieValue;
}
