from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<news_id>[0-9]+)/(?P<news_link>.*)/postComment/$', views.post_comment, name='post_comment'),
    url(r'^(?P<news_id>[0-9]+)/(?P<news_link>.*)/$', views.get_content, name='get_content'),
    url(r'^register/$', views.register_user, name='register_user'),
    url(r'^login/$', views.login_user, name='login_user'),
    url(r'^logout/$', views.sign_out, name="sign_out"),
]
