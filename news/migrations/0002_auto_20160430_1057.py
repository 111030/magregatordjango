# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-04-30 10:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='news',
            old_name='news_content',
            new_name='news_abstract',
        ),
        migrations.RemoveField(
            model_name='news',
            name='id',
        ),
        migrations.AddField(
            model_name='news',
            name='news_link',
            field=models.CharField(default='zdravo', max_length=100, primary_key=True, serialize=False),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='news',
            name='news_date',
            field=models.DateField(blank=True),
        ),
    ]
