from bs4 import BeautifulSoup
import requests
from news.models import News
from . import checkContent

def crawlContent(news):

    if checkContent.checkContent(news):
        return "Error!!!"

    r = requests.get(news)
    soup = BeautifulSoup(r.content, "html5lib")

    main_content = soup.find(id="main_content")
    table = main_content.find("table")
    tbody = table.find("tbody")
    tr = tbody.find("tr")
    td = tr.find("td")
    return td.prettify()
