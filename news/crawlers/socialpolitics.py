import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    n_url = "http://www.mtsp.gov.mk/"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib")
    #questions = []

    main_hold = soup.find("div", class_="main-hold")
    centered = main_hold.find_all("div", class_="centered")[1]
    main_cont = centered.find("div", class_="main-cont")
    news_content = main_cont.find("div", class_="news-content")
    content = news_content.find("div", class_="content")
    previews = content.find_all("div", class_="preview")
    for preview in previews:
        pdate = preview.find("p")
        # pdate.string
        title = preview.find("h2", class_="title")
        # title.string
        h3img = preview.find("h3")
        # h3img.string
        if h3img:
            img = h3img.find("img")
        # img['src']
        a = preview.find("a")
        # a['href']
        if h3img and img and pdate:
            question = {'img': toLink.toValidLink(n_url, img['src']),
                        'href': toLink.toValidLink(n_url, a['href']),
                        'title': title.string,
                        'abstract': h3img.get_text(),
                        'date': extractDate.extract(pdate.string)}
            if existsAndDate.check(question):
                insertQuestion.insert(n_url, question)
               # questions.append(question)
   # return questions
