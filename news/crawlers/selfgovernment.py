import requests
import urllib
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    n_url = "http://mls.gov.mk/mk/"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib")
    #questions = []

    maincontent = soup.find(id="jm-maincontent")
    item_page = maincontent.find("div", class_="item-page")
    article_body = item_page.find("div", attrs={'itemprop': "articleBody"})
    moduletable = article_body.find("div", class_="moduletable")
    divmodule = moduletable.find("div")
    slide = divmodule.find("div", class_="slide")
    bt_row = slide.find_all("div", class_="bt-row")
    for row in bt_row:
        bt_inner = row.find("div", class_="bt-inner")
        title = bt_inner.find("a", class_="bt-title")
        # title.string
        # title['href']
        # title.string
        center = bt_inner.find("div", class_="bt-center")
        acenter = center.find("a")
        imga = acenter.find("img")
        # imga['src']
        extra = bt_inner.find("div", class_="bt-extra")
        bt_date = extra.find("span", class_="bt-date")
        # bt_date.string
        question = {'img': imga['src'],
                    'href': toLink.toValidLink(n_url, title['href']),
                    'title': title.string,
                    'abstract': title.string,
                    'date': extractDate.extract(bt_date.string)}
        if existsAndDate.check(question):
            insertQuestion.insert(n_url, question)
            #questions.append(question)
    #return questions
