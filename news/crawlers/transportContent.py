from bs4 import BeautifulSoup
import requests
from news.models import News
from . import checkContent

def crawlContent(news):

    if checkContent.checkContent(news):
        return "Error!!!"

    r = requests.get(news)
    soup = BeautifulSoup(r.content, "html5lib")

    t3content = soup.find(id="t3-content")
    article = t3content.find("article", class_="article")
    articlefull = article.find("section", class_="article-full")
    articlecontentmain = articlefull.find("div", class_="article-content-main")
    articlecontent = articlecontentmain.find("section",class_="article-content")
    return articlecontent.prettify()     
