from bs4 import BeautifulSoup
import requests
from news.models import News
from . import checkContent

def crawlContent(news):

    if checkContent.checkContent(news):
        return "Error!!!"

    r = requests.get(news)
    soup = BeautifulSoup(r.content, "html5lib", from_encoding="utf-8")

    component = soup.find(id="component")
    return component.prettify()

