import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    s_url = "http://www.mzsv.gov.mk/"
    n_url = "http://www.mzsv.gov.mk/?q=node"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib")
    #questions = []

    main_content = soup.find(id="main_content")
    table = main_content.find("table")
    tbody = table.find("tbody")
    tr = tbody.find("tr")
    td = tr.find("td")
    nodes = td.find_all("div", class_="node")
    for node in nodes:
        title = node.find("div", class_="title")
        # title.string
        content = node.find("div", class_="content")
        field_data = content.find("div", class_="field-field-data")
        date_display = field_data.find("span", class_="date-display-single")
        # date_display.string
        field_slika = content.find("div", class_="field-field-slika")
        if field_slika:
            img = field_slika.find("img")
        # img['src']
        p = content.find("p")
        # p.string
        links = node.find("div", class_="links")
        if links:
            ulinks = links.find("ul", class_="links")
            read_more = ulinks.find("li", class_="node_read_more")
            aread_more = read_more.find("a", class_="node_read_more")
            # a['href']
        if field_slika and links:
            question = {'img': img['src'],
                        'href': toLink.toValidLink(s_url, aread_more['href']),
                        'title': title.string,
                        'abstract': p.string,
                        'date': extractDate.extract(date_display.string)}
            if existsAndDate.check(question):
                insertQuestion.insert(s_url, question)
               # questions.append(question)
   # return questions
