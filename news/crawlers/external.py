import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    s_url = "http://www.mfa.gov.mk/"
    n_url = "http://www.mfa.gov.mk/index.php/mk/"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib", from_encoding="utf-8")
    #questions = []

    pos_above_body = soup.find(id="s5_pos_above_body_1")
    btcontentslider = pos_above_body.find(id="btcontentslider521")
    slides_container = btcontentslider.find("div", class_="slides_container")
    # slides_control = slides_container.find("div", class_="slides_control")
    slides = slides_container.find_all("div", class_="slide")
    for slide in slides:
        bt = slide.find("div", class_="bt-row bt-row-first")
        bt_inner = bt.find("div", class_="bt-inner")
        title = bt_inner.find("a", class_="bt-title")
        # title['href']
        # title.string
        center = bt_inner.find("div", class_="bt-center")
        acenter = center.find("a", class_="bt-image-link")
        imga = acenter.find("img")
        # imga['src']
        introtext = bt_inner.find("div", class_="bt-introtext")
        # introtext.string
        r2 = requests.get(toLink.toValidLink(s_url, title['href']))
        soup2 = BeautifulSoup(r2.content, "html5lib")
        wrap_inner = soup2.find(id="s5_component_wrap_inner")
        item_page = wrap_inner.find("div", class_="item-page")
        article_info = item_page.find("div", class_="article-info")
        dlarticle_info = article_info.find("dl", class_="article-info")
        published = dlarticle_info.find("dd", class_="published")
        # published.string
        question = {'img': imga['src'],
                    'href': toLink.toValidLink(s_url, title['href']),
                    'title': title.get_text(),
                    'abstract': introtext.get_text(),
                    'date': extractDate.extract(published.get_text())}
        if existsAndDate.check(question):
            insertQuestion.insert(n_url, question)
            #questions.append(question)
    #return questions
