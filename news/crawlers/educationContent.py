from bs4 import BeautifulSoup
import requests
from news.models import News
from . import checkContent

def crawlContent(news):

    if checkContent.checkContent(news):
        return "Error!!!"

    r = requests.get(news)
    soup = BeautifulSoup(r.content, "html5lib", from_encoding="utf-8")

    t3content = soup.find(id="t3-content")
    article = t3content.find("article")
    return article.prettify()
