import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    n_url = "http://www.mioa.gov.mk/"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib")
    #questions = []
    

    main_content = soup.find(id="main_content")
    mctable = main_content.find("table")
    tablebody = mctable.find("tbody")
    tablebodytd = tablebody.find("td")
    view_frontpage = tablebodytd.find("div", class_="view-frontpage")
    view_c_frontpage = view_frontpage.find("div", class_="view-content-frontpage")
    nodes = view_c_frontpage.find_all("div", class_="node")
    for i in range(1, len(nodes)):
        title = nodes[i].find("span", class_="title")
        # title.string
        content = nodes[i].find("div", class_="content")
        ps = content.find_all("p")
        pdate = ps[0].find("strong")
        # pdate.string
        pimga = ps[1].find("a")
        imga = pimga.find("img")
        # imga['src']
        pabstr = ""
        if len(ps) > 2:
            for j in range(2, len(ps)):
                pabstr = pabstr + ps[j]
        # pabstr.string
        links = nodes[i].find("div", class_="links")
        ullinks = links.find("ul", class_="links")
        lilinks = ullinks.find("li", class_="node_read_more")
        alink = lilinks.find("a")
        # a['href']
        question = {'img': toLink.toValidLink(n_url, imga['src']),
                    'href': toLink.toValidLink(n_url, alink['href']),
                    'title': title.string,
                    'abstract': pabstr.string,
                    'date': extractDate.extract(pdate.string)}
        if existsAndDate.check(question):
            insertQuestion.insert(n_url, question)
            #questions.append(question)

    #return questions

