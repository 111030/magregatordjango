import requests
from bs4 import BeautifulSoup
from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    #questions = []
    s_url = "http://www.economy.gov.mk/"
    n_url = "http://www.economy.gov.mk/vesti"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib")

    contentbody = soup.find(id="contentbody")
    middle_bar = contentbody.find("div", class_="middle_bar")
    catg_nav = middle_bar.find("ul", class_="catg_nav")
    lis = catg_nav.find_all("li")
    for li in lis:
        slika = li.find("div", class_="slika")
        aslika = slika.find("a")
        aimg = aslika.find("img")
        # aimg['src']
        tekst = li.find("div", class_="tekst")
        btekst = tekst.find("b")
        catg_title = btekst.find("a", class_="catg_title")
        # catg_title['href']
        # catg_title.string
        ptekst = tekst.find("p")
        # ptekst.string
        sing_commentbox = tekst.find("div", class_="sing_commentbox")
        pdata = sing_commentbox.find("p")
        # pdata.string
        question = {'img': toLink.toValidLink(s_url, aimg['src']),
                    'href': toLink.toValidLink(s_url, catg_title['href']),
                    'title': catg_title.get_text(),
                    'abstract': ptekst.get_text(),
                    'date': extractDate.extract(str(pdata.get_text()))}
        if existsAndDate.check(question):
            insertQuestion.insert(s_url, question)
            #questions.append(question)

    #return questions
