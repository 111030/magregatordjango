import datetime

def extract(s):

    cyrilicMonths = {'јануари': 1,
                     'февруари': 2,
                     'март': 3,
                     'април': 4,
                     'мај': 5,
                     'јуни': 6,
                     'јули': 7,
                     'август': 8,
                     'септември': 9,
                     'октомври': 10,
                     'ноември': 11,
                     'декември': 12}

    engMonths = {'january': 1,
                 'february': 2,
                 'march': 3,
                 'april': 4,
                 'may': 5,
                 'june': 6,
                 'july': 7,
                 'august': 8,
                 'september': 9,
                 'october': 10,
                 'november': 11,
                 'december': 12}
    
    s_parted = s.split()
    if len(s_parted) == 1:
        date_cross = str(s_parted).split('-')
        if len(date_cross) == 3:
            return datetime.date(int(date_cross[2].strip("']")), int(date_cross[1]), int(date_cross[0].strip("['")))
        else:
            date_cross = str(s_parted).split('.')
            return datetime.date(int(date_cross[2].strip("']")), int(date_cross[1]), int(date_cross[0].strip("['")))


    else:
        if len(s_parted) == 3:
            return datetime.date(int(s_parted[2]), cyrilicMonths[s_parted[1].lower()], int(s_parted[0]))
        elif len(s_parted) == 4:

            return datetime.date(int(s_parted[3]), cyrilicMonths[s_parted[2].lower()], int(s_parted[1]))
        else:
            day = s_parted[3].split(',')[0]
            return datetime.date(int(s_parted[4]), engMonths[s_parted[2].lower()], int(day))
# Everything done


