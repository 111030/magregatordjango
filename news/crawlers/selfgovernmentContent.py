from bs4 import BeautifulSoup
import requests
from news.models import News
from . import checkContent

def crawlContent(news):

    if checkContent.checkContent(news):
        return "Error!!!"

    r = requests.get(news)
    soup = BeautifulSoup(r.content, "html5lib", from_encoding="utf-8")

    jmcontent = soup.find(id="jm-content")
    itempage = jmcontent.find("div", class_="item-page")
    return itempage.prettify()
