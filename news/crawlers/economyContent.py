import requests
from bs4 import BeautifulSoup
from . import checkContent

def crawlContent(news):
    
    if checkContent.checkContent(news):
        return "Error!!!"
    
    r = requests.get(news)
    soup = BeautifulSoup(r.content, "html5lib")

    contentbody = soup.find(id="contentbody")
    middle_bar = contentbody.find("div", class_="middle_bar")
    post = middle_bar.find("div", class_="single_category post")
    return post.prettify()
