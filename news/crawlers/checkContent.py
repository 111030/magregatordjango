from news.models import News

def checkContent(news_link):
    try:
        news_c = News.objects.get(pk=news_link)
        if news_c and news_c.news_content:
            return True
        else:
            return False
    except News.DoesNotExist:
        return True

