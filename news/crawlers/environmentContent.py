from bs4 import BeautifulSoup
import requests
from . import checkContent

def crawlContent(news):

    if checkContent.checkContent(news):
        return "Error!!!"

    r = requests.get(news)
    soup = BeautifulSoup(r.content, "html5lib")

    content = soup.find(id="content")
    nastani = content.find("article", class_="nastani")
    return nastani.prettify()
