from news.models import News
import datetime

def check(question):
    tempQues = News.objects.filter(news_link=question['href'])
    if tempQues:
        return False
    if question['date'] <= datetime.date.today() - datetime.timedelta(days=30):
        return False
    return True
        

