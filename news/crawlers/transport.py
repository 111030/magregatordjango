import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    n_url = "http://www.mtc.gov.mk/"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib")
    #questions = []

    mainbody = soup.find(id="t3-mainbody")
    magazine_list = mainbody.find("div", class_="magazine-list")
    magazine_cats = magazine_list.find("div", class_="magazine-categories")
    magazine_cat = magazine_cats.find("div", class_="magazine-category")
    row_articles = magazine_cat.find("div", class_="row-articles")
    magazines_items = row_articles.find_all("div", class_="magazine-item")
    for mag_item in magazine_items:
        item_media = mag_item.find("div", class_="magazine-item-media")
        item_image = item_media.find("div", class_="item-image")
        aimage = item_image.find("a")
        img = aimage.find("img")
        # img['src']
        item_main = mag_item.find("div", class_="magazine-item-main")
        article_item = item_main.find("div", class_="article-title")
        h3 = article_item.find("h3")
        ah3 = h3.find("a")
        # ah3['href']
        # ah3.string
        article_aside = item_main.find("aside", class_="article-aside")
        article_info = article_aside.find("dl", class_="article-info")
        published = article_info.find("dd", class_="published")
        time = published.find("time")
        # time.string
        item_ct = item_main.find("div", class_="magazine-item-ct")
        pitem = item_ct.find("p")
        # pitem.string
        question = {'img': toLink.toValidLink(n_url, img['src']),
                    'href': toLink.toValidLink(n_url, ah3['href']),
                    'title': ah3.string,
                    'abstract': pitem.string,
                    'date': extractDate.extract(time.string)}
        if existsAndDate.check(question):
            insertQuestion.insert(n_url, question)
            #questions.append(question)


    #return questions
