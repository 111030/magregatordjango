from bs4 import BeautifulSoup
import requests
from news.models import News
from . import checkContent

def crawlContent(news):

    if checkContent.checkContent(news):
        return "Error!!!"

    r = requests.get(news)
    soup = BeautifulSoup(r.content, "html5lib")

    s5cwi = soup.find(id="s5_component_wrap_inner")
    itempage = s5cwi.find("div", class_="item-page")
    return itempage.prettify()
