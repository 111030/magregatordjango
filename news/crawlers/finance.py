import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    #questions = []
    n_url = "http://www.finance.gov.mk/"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib")

    content = soup.find(id="content")
    block_news = content.find(id="block_news")
    block_views = block_news.find(id="block-views-block-news-block")
    contentc = block_views.find("div", class_="content")
    view = contentc.find("div", class_="view")
    view_content = view.find("div", class_="view-content")
    views_rows = view_content.find_all("div", class_="views-row")
    for row in views_rows:
        thumb = row.find("div",class_="views-field-field-thumbnail")
        athumb = thumb.find("a")
        # athumb['href']
        aimg = athumb.find("img")
        # aimg['src']
        created = row.find("div", class_="views-field-created")
        contentcreated = created.find("span", class_="field-content")
        # contentcreated.string
        title = row.find("span", class_="views-field-title")
        title_content = title.find("span", class_="field-content")
        atitle = title_content.find("a")
        # title.string
        field_body = row.find("div", class_="views-field-body")
        body_content = field_body.find("div", class_="field-content")
        body_contentp = body_content.find("p")
        # p.string
        question = {'img': aimg['src'],
                    'href': toLink.toValidLink(n_url, athumb['href']),
                    'title': atitle.string,
                    'abstract': body_contentp.get_text(),
                    'date': extractDate.extract(contentcreated.string)}
        if existsAndDate.check(question):
            insertQuestion.insert(n_url, question)
           # questions.append(question)
    #return questions
