import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    n_url = "http://www.kultura.gov.mk/"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib")
    #questions = []

    content_top = soup.find(id="content_top")
    newspro3 = content_top.find(id="gk_npro-newspro3")
    npro_full = newspro3.find("div", class_="gk_npro_full")
    scroll1 = npro_full.find("div", class_="gk_npro_full_scroll1")
    scroll2 = scroll1.find("div", class_="gk_npro_full_scroll2")
    tablewrap = scroll2.find("div", class_="gk_npro_full_tablewrap")
    npro_table = tablewrap.find("table", class_="gk_npro_table")
    tbody = npro_table.find("tbody")
    trs = tbody.find_all("tr")
    for tr in trs:
        td = tr.find("td")
        nspro_bg_wrap = td.find("div", class_="nspro_bg_wrap")
        header = nspro_bg_wrap.find("h4", class_="gk_npro_header")
        aheader = header.find("a")
        # aheader['href']
        # aheader.string
        info = nspro_bg_wrap.find("p", class_="gk_npro_info")
        # info.string
        image_static = nspro_bg_wrap.find("img", class_="gk_npro_image_static")
        # image_static['src']
        npro_text = nspro_bg_wrap.find("p", class_="gk_npro_text")
        # npro_text.string
        if image_static:
            question = {'img': toLink.toValidLink(n_url, image_static['src']),
                        'href': toLink.toValidLink(n_url, aheader['href']),
                        'title': aheader.string,
                        'abstract': npro_text.get_text(),
                        'date': extractDate.extract(info.string)}
            if existsAndDate.check(question):
                insertQuestion.insert(n_url, question)
                #questions.append(question)
    #return questions
