import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    n_url = "http://www.moepp.gov.mk/"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib", from_encoding="utf-8")
    #questions = []

    con = soup.find(id="content")
    containers = con.find_all("div", class_="post-container")
    for cont in containers:
        thumb = cont.find("div", class_="post-thumb")
        img = thumb.find(img)
        # img['src']
        post_content = cont.find("div", class_="post-content")
        title = post_content.find("div", class_="post-title")
        a = title.find("a")
        # a['href']
        # a.string
        postdate = post_content.find("div", class_="postdate")
        # postdate.string
        # cont.string
        question = {'img': img['src'],
                    'href': a['href'],
                    'title': a.string,
                    'abstract': cont.string,
                    'date': extractDate.extract(postdate.string)}
        if existsAndDate.check(question):
            insertQuestion.insert(n_url, question)
            #questions.append(question)
    #return questions
