import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    n_url = "http://www.mon.gov.mk/"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib", from_encoding="utf-8")
    #questions = []

    mainbody = soup.find(id="t3-mainbody")
    content = mainbody.find(id="t3-content")
    contentmasstop = content.find("div", class_="content-mass-top")
    module = contentmasstop.find("div", class_="t3-module module ")
    inner = module.find("div", class_="module-inner")
    modulect = inner.find("div", class_="module-ct")
    sidenews = modulect.find("div", class_="ja-sidenews-list clearfix")
    slidenews = sidenews.find_all("div", class_="ja-slidenews-item")
    for slide in slidenews:
        img = slide.find("img")
        # img['src']
        title = slide.find("a", class_="ja-title")
        # title['href']
        # title.string
        createdate = slide.find("span", class_="ja-createdate")
        # createdate.string
        # slide.string
        question = {'img': toLink.toValidLink(n_url, img['src']),
                    'href': toLink.toValidLink(n_url, title['href']),
                    'title': title.string,
                    'abstract': slide.get_text(),
                    'date': extractDate.extract(createdate.string)}
        if existsAndDate.check(question):
            insertQuestion.insert(n_url, question)
            #questions.append(question)
    #return questions
