import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    s_url = "http://www.morm.gov.mk/"
    n_url = "http://www.morm.gov.mk/?post_type=mainnews&lang=mk"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib")
    #questions = []

    content = soup.find(id="content")
    hfeed = content.find("div", class_="hfeed")
    mainnews = content.find_all("div", class_="mainnews")
    for entry in mainnews:
        # аo = entry.find("a")
        imga = entry.find("img")
        # imga['src']
        h2 = entry.find("h2", "mainnews-title entry-title")
        ah2 = h2.find("a")
        # ah2['href']
        # ah2.string
        byline = entry.find("div", class_="byline")
        published = byline.find("abbr", class_="published")
        # published.string
        summary = entry.find("div", class_="entry-summary")
        psummary = summary.find("p")
        # psummary.string
        if imga and psummary:
            question = {'img': imga['src'],
                        'href': ah2['href'],
                        'title': ah2.string,
                        'abstract': psummary.string,
                        'date': extractDate.extract(published.string)}
            if existsAndDate.check(question):
                insertQuestion.insert(s_url, question)
                #questions.append(question)
    #return questions
