from bs4 import BeautifulSoup
import requests
from . import checkContent

def crawlContent(news):

    if checkContent.checkContent(news):
        return "Error!!!"

    r = requests.get(news)
    soup = BeautifulSoup(r.content, "html5lib")

    main_content = soup.find(id="main_content")
    content = main_content.find("div", class_="content")
    return content.prettify()
