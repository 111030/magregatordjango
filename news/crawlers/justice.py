import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    n_url = "http://www.pravda.gov.mk/"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib")
    #questions = []

    gradientbg = soup.find(id="gradientbg")
    center = gradientbg.find("center")
    table = center.find("table")
    tbody = table.find("tbody")
    trfour = tbody.content
    td = trfour[4].content.find("td")
    table = td.find("table")
    tbody = table.find("tbody")
    tr = tbody.find("tr")
    td = tr.find("td")
    table = td.find("table")
    tbody = table.find("tbody")
    trs = tbody.find_all("tr")
    for row in trs:
        kelija = []
        kelija[0] = row.find("td", class_="kelija1")
        kelija[1] = row.find("td", class_="kelija2")
        for cell in kelija:
            table = cell.find("table")
            tbody = table.find("tbody")
            entries = tbody.find_all("tr")
            novosti_datum = tr[0].find("td", class_="novosti_datum")
            # novosti_datum.string
            novosti_naslov = tr[1].find("td", class_="novosti_naslov")
            # novosti_naslov.string
            novosti_tekst = tr[2].find("td", class_="novosti_tekst")
            table = novosti_tekst.find("table")
            tbody = table.find("tbody")
            trtemp = tbody.find("tr")
            td = trtemp.find_all("td")[0]
            img = td.find("img")
            # link + img['src']
            td = trtemp.find_all("td")[1]
            # td.string
            novosti_tekst = tr[3].find("td", class_="novosti_tekst")
            anovosti = novosti_tekst.find("a")
            # anovosti['href']
            question = {'img': toLink.toValidLink(n_url, img['src']),
                        'href': toLink.toValidLink(n_url, anovosti['href']),
                        'title': novosti_naslov.string,
                        'abstract': td.string,
                        'date': extractDate.extract(novosti_datum.string)}
            if existsAndDate.check(question):
                insertQuestion.insert(n_url, question)
               # questions.append(question)
   # return questions
