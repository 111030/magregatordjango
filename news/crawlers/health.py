import requests
from bs4 import BeautifulSoup

from . import toLink
from . import extractDate
from . import existsAndDate
from . import insertQuestion

def crawl():
    
    n_url = "http://zdravstvo.gov.mk/"
    r = requests.get(n_url)
    soup = BeautifulSoup(r.content, "html5lib")
   # questions = []

    content = soup.find(id="content")
    cats = content.find("div", class_="cats-by-2")
    posts_stacked = cats.find("div", class_="cat-posts-stacked")
    posts = posts_stacked.find_all("div", class_="post")
    for post in posts:
        aimg = post.find("a")
        # aimg['href']
        thumbnail = aimg.find("img", class_="thumbnail")
        # thumbnail['src']
        entry = post.find("div", class_="entry")
        title = entry.find("h3", class_="post-title")
        atitle = title.find("a")
        # atitle.string
        excerpt = entry.find("div", class_="excerpt")
        p = excerpt.find("p")
        # p.string
        meta = entry.find("div", class_="meta")
        date = meta.find("span", class_="meta-date")
        # date.string
        question = {'img': thumbnail['src'],
                    'href': aimg['href'],
                    'title': atitle.string,
                    'abstract': p.string,
                    'date': extractDate.extract(date.string)}
        if existsAndDate.check(question):
            insertQuestion.insert(n_url, question)
           # questions.append(question)
   # return questions

