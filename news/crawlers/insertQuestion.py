from news.models import News, NewsSource

def insert(filter_url, question):
    newsSource = NewsSource.objects.filter(link_source=filter_url)
    q = newsSource[0].news_set.create(news_link=question['href'], news_title=question['title'], news_date=question['date'], news_abstract=question['abstract'], news_piclink=question['img'])
    q.save()
