def toValidLink(link1, link2):
    if link1[len(link1)-1]=="/" and link2[0]=="/":
        return link1[:len(link1)-1] + "/" + link2[1:len(link2)]
    elif link1[len(link1)-1]!="/" and link2[0]!="/":
        return link1 + "/" + link2
    else:
        return link1 + link2

