from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from bs4 import BeautifulSoup

from .models import News, NewsSource, Comment
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login
from news.crawlers import *
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


# Create your views here.


def index(request):
    deleteOld()
    all_links = NewsSource.objects.all()
    #qs = []
    economy.crawl()
    finance.crawl()
    #informatics.crawl()
    culture.crawl()
    selfgovernment.crawl()
    external.crawl()
    education.crawl()
    defense.crawl()
    #justice.crawl()
    #transport.crawl()
    socialpolitics.crawl()
    health.crawl()
    agrifortreconom.crawl()
    #environment.crawl()


    questions = []
    #for question in qs:
        #for news in question:
            #questions.append(news)
    #someSort(questions)
    questions = News.objects.order_by('-news_date')
    context = {'all_links': all_links, 'questions': questions}
    logout(request)
    return render(request, 'news/index.html', context)

def someSort(questionlist):
    for i in range(0, len(questionlist)):
        j = i
        while j > 0 and (questionlist[j-1])['date'] < (questionlist[j])['date']:
            temp = (questionlist[j])['date']
            (questionlist[j])['date'] = (questionlist[j-1])['date']
            (questionlist[j-1])['date'] = temp
            j = j - 1
    for i in range(0, len(questionlist)):
        (questionlist[i])['date'] = (questionlist[i])['date'].strftime("%d.%m.%Y")

def deleteOld():
    qs = News.objects.all()
    for q in qs:
        if q.is_old():
            q.delete()

def get_content(request, news_id, news_link):
    all_links = NewsSource.objects.all()
    news_sources = all_links.get(pk=news_id)
    content = ""
    if news_sources.link_source.find("http://www.economy.gov.mk/") != -1:
        content = economyContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://www.finance.gov.mk/") != -1:
        content = financeContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://www.mioa.gov.mk/") != -1:
        content = informaticsContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://www.kultura.gov.mk/") != -1:
        content = cultureContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://mls.gov.mk/mk/") != -1:
        content = selfgovernmentContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://www.mfa.gov.mk/index.php/mk/") != -1:
        content = externalContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://www.mon.gov.mk/") != -1:
        content = educationContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://www.morm.gov.mk/") != -1:
        content = defenseContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://www.pravda.gov.mk/") != -1:
        content = justiceContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://www.mtc.gov.mk/") != -1:
        content = transportContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://www.mtsp.gov.mk/") != -1:
        content = socialpoliticsContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://zdravstvo.gov.mk/") != -1:
        content = healthContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://www.mzsv.gov.mk/") != -1:
        content = agrifortreconomContent.crawlContent(news_link)
    elif news_sources.link_source.find("http://www.moepp.gov.mk/") != -1:
        content = environmentContent.crawlContent(news_link)
    else:
        content = news_link
    context = ""
    try:
        current_news = News.objects.get(news_link=news_link)
        if request.user.is_authenticated():
            comments = current_news.comment_set.order_by('-comment_date')
            user_name = request.user.username
            context = {'user': user_name, 'comments': comments, 'content': content}
        else:
            context = {'all_links': all_links, 'content': content}
        return render(request, 'news/content.html', context)
    except News.DoesNotExist:
        context = {'all_links': all_links, 'content': content}
        return render(request, 'news/content.html', context)

def register_user(request):
    
    m_username = request.POST['r_username']
    m_password = request.POST['r_password']
    m_email = request.POST['email']
    m_first_name = request.POST['name']
    m_last_name = request.POST['surname']

    try:
        user = User.objects.get(username=m_username)
        return JsonResponse({'error_message': 'Username already exists'})

    except User.DoesNotExist:
            user = User.objects.create_user(username=m_username, password=m_password, email=m_email, first_name=m_first_name, last_name=m_last_name)
            user.save()
            return JsonResponse({'error_message': 'Success'})
            
def login_user(request):
    l_username = request.POST['username']
    l_password = request.POST['password']
    user = authenticate(username=l_username, password=l_password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return JsonResponse({'username': l_username})
    else:
        return JsonResponse({'password_error': 'User does not exists: {0},{1}'.format(l_username, l_password)})

def post_comment(request, news_id, news_link):
    username = request.POST['username']
    comment = request.POST['comment']
    n = News.objects.get(news_link=news_link)
    u = User.objects.get(username=username)
    c = n.comment_set.create(comment_user=u, comment_text=comment)
    c.save()
    response = HttpResponse()
    response.write("<div class='comment_row'>")
    response.write("<div class='row'>")
    response.write("<div class='col-md-3'>")
    response.write("<p>"+c.comment_user.username+"</p>")
    response.write("</div>")
    response.write("<div class='col-md-9'>")
    response.write("<p>"+c.comment_text+"</p>")
    response.write("</div>")
    response.write("</div>")
    response.write("<div class='row'>")
    response.write("<div class='col-md-3'>")
    response.write("</div>")
    response.write("<div class='col-md-9'>")
    response.write("<div class='row'>")
    response.write("<div class='col-md-3' style='float: left;'>")
    response.write("<span class='glyphicon glyphicon-thumbs-up'></span>")
    response.write("<span>"+str(c.comment_likes)+"</span>")
    response.write("</div>")
    response.write("<div class='col-md-9' style='float: right;'>")
    response.write("<span>Objaveno na: </span>")
    response.write("<span>"+str(c.comment_date)+"</span>")
    response.write("</div>")
    response.write("</div>")
    response.write("</div>")
    response.write("</div>")
    response.write("</div>")
    return response

def sign_out(request):
    logout(request)
    return HttpResponse() 
