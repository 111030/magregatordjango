from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.


class NewsSource(models.Model):
    link_source = models.CharField(max_length=50)
    link_news = models.CharField(max_length=100)
    name_source = models.CharField(max_length=200)
    name_label = models.CharField(max_length=100)
    def __str__(self):
        return '{0}; {1}; {2}; {3}'.format(self.link_source, self.link_news, self.name_source, self.name_label)


class News(models.Model):
    news_source = models.ForeignKey(NewsSource, on_delete=models.CASCADE)
    news_link = models.CharField(max_length=100, primary_key=True)
    news_title = models.CharField(max_length=250)
    news_date = models.DateField(blank=True)
    news_abstract = models.CharField(max_length=750, blank=True)
    news_piclink = models.CharField(max_length=200, blank=True)
    news_likescount = models.IntegerField(default=0)
    news_content = models.CharField(max_length=10000, blank=True)
    def is_old(self):
        return self.news_date <= datetime.date.today() - datetime.timedelta(days=30)


class Comment(models.Model):
    comment_user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment_news = models.ForeignKey(News, on_delete=models.CASCADE)
    comment_date = models.DateTimeField(auto_now_add=True)
    comment_text = models.CharField(max_length=1000)
    comment_likes = models.IntegerField(default=0)


    
    
